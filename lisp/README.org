


* Compatibility

  Build a new Lisp Machine is a bit of a mess. This should build
  against the usim tagged 0.0.1.  The instructions that follow will
  get you to the point of building a new version of the microcode and
  zemacs.  The entire sources does compile, but will not load
  successfully, perhaps because of bugs in the the emulator or a
  mismatch between micro-code and source.

  Currently, the build process has only been tested against Ubuntu
  (14.x and 15.x).



** Building a disk image

   Follow the instructions in usim for building the sources, make sure
   that the disk template that you building against is for
   template.disk4 

** Set up Lisp

   The contents of this directory currently should be copied to the
   directory /tree  
   

   #+BEGIN_SRC sh 
     sudo mkdir /tree
     chown -R <my-id>:<my-id> /tree
     cp * /tree
   #+END_SRC

   Replacing <my-id> with a user id with a login

** Start usim with the with chaos net support

   From the usim folder 

   #+BEGIN_SRC sh 
     ./launch-usim.sh
   #+END_SRC

   This should launch the Lisp Machine emulator

*** Log in

    Once the Lisp Machine os has finished booting, you'll be prompted
    for the date and time.  The values you enter for this are
    important because the Lisp Machine uses both values to time stamp
    files, which in turn is used to determine whether files need to be
    recompiled or loaded.  Below is an example of what you might type:

    11/25/2015 12:03

    Note that the Lisp Machine is not Y2K compliant.  But it is able
    to keep track of relative dates past the year 2000, though they
    will print incorrectly in some contexts.


    Once you've entered the current date & time in you'll be greeted
    with a hearald printing the versions of the software installed.
    The final line should read:

    Distribution unknown, with associated machine UNKNOWN

   *** Load utility file

    Note, the Lisp Machine command line does not present you with a
    prompt.  Also, s-expressions are evaluated the moment you type the
    final crossing parenthesis.  First load the utility file by typing
    the following:

    (si:set-sys-host "server" ':unix 404)

    If you are prompted with the question:

    Host is server, ok? (Y or N) 

    respond by pressing 'y'

    Note, if you're familiar with Common Lisp, you'll notice that
    keywords have a slightly different format: they're appended with a
    single quote followed by a colon in the current version of Zeta
    Lisp.  By typing the above, you're causing the current session to
    become 'network aware'

    Now load the utility file:

    (load "server:////tree//lispm-bot//copy.lisp")

    Zeta Lisp treats the forward slash character as an escape
    character, in much the same way that Common Lisp uses the back
    slash character.  So, instead of
    server://tree/lispm-bot/copy.lisp, each forward slash must be
    escaped by doubling them.

    You'll be asked to log into the lisp machine.  To do this type
    
    lispm-bot t

    This will set your current user to lispm-bot.  The 't' suppresses
    executing the lispm-bot initialization file.  (which shouldn't
    exist anyway.)

    You will then be prompted for you unix username and password.
    This is the same userid and password you use to log into Ubuntu.

    You'll then be prompted if you want to to create the BRAD
    package.  Reply: yes
    
*** Copy sources

    Type the following expression.

    #+BEGIN_SRC lisp
      (brad:init-and-copy-src)
    #+END_SRC

    You'll now be presented with several dire warnings to which you
    need to answer 'yes'.  (As if that has ever prevented someone from
    deleting the contents of their root directory or otherwise
    destoying their sistem -- some things never change.)

    You'll see a lot of activity on the screen relating to copying
    files.  Wait until all activity has stopped.

*** Create the folder >ubin using the FS Filesystem tool


** Restart and build a new version of the microcode
   1) Restart usim
      
      Currently impolitely kill usim by closing the Ubuntu Window. 

      (TODO: this can put your system in an inconsistent state and
      make the the lm unbootable.  This has only happened to me when
      terminating the machine while it's booting up.  But presumabler
      there are other unlucky states that could cause problems.)


      Relaunch usim using the same command we used previously.

      #+BEGIN_SRC sh 
        ./launch-usim.sh
      #+END_SRC

   2) Login in in the same way as above using the lispm-bot

   3) Kickoff script to assemble micro-code

      #+BEGIN_SRC lisp
        (load "local:>lispm-bot>assemble-microcode.lisp")
        (assemble-microcode)
      #+END_SRC

      Accept the default given for writing the CWARNS.text file.
      
      select local:>lispm-bot> 
      as the directory for all mcr artifacts

      (TODO: add ubin as a logical host.)


  README
  10/13/04

  These are images of 4 ITS backup tapes containing lisp machine source
  code.  This is an early snapshot from the MIT AI labs, about the time
  Symbolics was formed.  This software is designed to run on the CADR
  lisp machine.  The system version of this software is "46".

  This software is licensed by the Massachusetts Institute of Technology.
  Please read the LICENSE file.

  The tapes are labeled:

	  Lisp Machine System Software
	  Reel 1,2,3,4 10/17/81	
	  9 track - 800bpi	
	  ITS dump format	

  To read them you need to :

  1) convert them from their existing format to "simh" format
  2) use 'itstar' to extract the files

  Programs to do this are included.

  Note that the tape2 is "damaged" because the first block is unreable.
  We forward spaced file and then bsf'd and bsr'd (backspace record)
  about 40 times and then started the image program.  In order to make
  itstar happy I spliced 20 bytes on the front of it (see tpc2simh.c).
  I believe we only lost one file, a SUDS file for the chaosnet board.

  I am working to recover this tape and hope to have a fixed image
  shortly.

  ---

  Credits:

  I would like to thank Tom Knight for working with the PTO office at
  MIT to secure permission to release these tape.  Howard Shrobe was
  very helpful and encouraging.  Richard Greenblatt answered a lot of
  dumb questions and let me look in his garage.  Danial Weinreb found
  the tapes in his basement and provided some helpful insights.

  Brad Parker
  brad@heeltoe.com

  ---

  Notes on what is in the sources:

  From some 'chinual' source files

  Lisp Machine Manual
  Second Preliminary Version
  January 1979
  Daniel Weinreb
  David Moon

  This report describes research done at the Artificial Intelligence
  Laboratory of the Massachusetts Institute of Technology.  Support for
  the laboratory's artificial intelligence research is provided in part
  by the Advanced Research Projects Agency of the Department of Defense
  under Office of Naval Research Contract number N00014-75-C-0643.

  (c) Copyright by the Massachusetts Institute of Technology; Cambridge,
  Mass. 02139 All rights reserved.

  system.versin	- says "46"
  promh.9		- prom version 9
  ucadr.694	- micrcode version 694

  The SUDS files appear to be for the "cadr4" schematics.

  ---

  FILES IN THIS RELEASE:

  README				this file
  LICENSE				MIT license text - please read!

  doit.sh				script to extract sources
  tpc2simh.c			program to convert tpc to simh format
  itstar.tar.gz			ITS back tape reader

  lmss_itsdump_101780.tar.gz	extracted sources tar file
  extract				extracted source dir

  Original "tpc" tape images:

  -rw-rw-r--    1 brad     brad     15231846 Oct 12 19:39 mit1.tpc
  -rw-rw-r--    1 brad     brad     15446387 Oct 12 20:05 mit2.tpc
  -rw-rw-r--    1 brad     brad     15176474 Oct 12 20:19 mit3.tpc
  -rw-rw-r--    1 brad     brad     11922791 Oct 12 20:34 mit4.tpc

  "tpc" images converted to "simh" format:

  -rw-rw-r--    1 brad     brad     15214196 Oct 12 23:07 mit1.simh
  -rw-rw-r--    1 brad     brad     15431140 Oct 12 23:25 mit2.simh
  -rw-rw-r--    1 brad     brad     15158636 Oct 12 23:07 mit3.simh
  -rw-rw-r--    1 brad     brad     11909344 Oct 12 23:09 mit4.simh

