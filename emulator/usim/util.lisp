
(defparameter *block-size* (* 4 256))
(defparameter
    *disk-img-filename*
  "~/dev-local/mit-cadr/emulator/usim/disk.img")

(defun get-block (index)
  (with-open-file (stream *disk-img-filename* :element-type 'unsigned-byte)
     (file-position stream (* index *block-size*))
    (loop for i from 0 to 256      ;; 256 Lispm words to the block
       for lisp-word = (list (read-byte stream) (read-byte stream)
			     (read-byte stream) (read-byte stream))
       collect lisp-word)))


(defun add-partition ()
  )




(defparameter s (with-open-file (stream "disk.img" :element-type 'unsigned-byte)
  (let ((sequence (make-sequence 'list (file-length stream))))
    (read-sequence sequence stream)
    sequence)))

(defparameter s
  (with-open-file (stream "disk.img" :element-type 'unsigned-byte)
    (let ((sequence (make-array (file-length stream) :adjustable t :element-type 'unsigned-byte)))
      (read-sequence sequence stream)
      sequence)))

(make-array 220024832 :adjustable t :element-type 'unsigned-byte)
