(ql:quickload :closer-mop)
(defparameter *default-template-label*
  '((:output "disk.img")
    (:label "LABL")
    (:cyls 815)
    (:heads 19)
    (:blockspertrack 17)
    (:mcr MCR1)
    (:lod LOD1)
    (:brand nil)
    (:text nil)
    (:comment "ucadr.mcr.841")))


;; units in cylinders?  If so2 cylynders to the block
;;
;;
;; 	cyls/head:    815
;;	heads/disk:   19
;;	blocks/track: 17
;;      bytes/block:  1024
;;
;; bytes/disk: 17 * 19 * 815: 269,562,880

(defparameter *default-template-partitions*
  '(((:label "MCR1")(:start #o21)(:length #o224)(:file-name "ucadr.mcr.841"))
    ((:label "MCR2")(:start #o245)(:length #o224)(:file-name "ucadr.mcr.979"))
    ((:label "PAGE")(:start #o524)(:length #o100000))
    ((:label "LOD1")(:start #o100524)(:length #o61400)(:file-name "partition-78.48-LOD1"))
    ((:label "LOD2")(:start #o162124)(:length #o61400)(:file-name "partition-sys210-LOD2"))
    ((:label "FILE")(:start #o243524)(:length #o70000))))


;; use MOP to avoid CLOS boilerplat code.


(closer-mop:ensure-class 'label :direct-slots (list (list :name 'mach)))

(defclass label ()
  ((label :accessor label :initarg label)
   (version :accessor version :initarg version)
   (cyls :accessor cyls :initarg cyls)))







   (ensure-class 'sst
  ':direct-superclasses '(plane)
  ':direct-slots (list (list ':name 'mach
                             ':readers '(mach-speed mach)
                             'mag-step '2
                             'locator '(sst-mach mach-location)))
  ':metaclass 'faster-class
  'another-option '(foo bar))
